#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 07:22:58 2021

@author: lucas
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn . metrics import classification_report
from sklearn . neighbors import KNeighborsClassifier
from sklearn.linear_model import Perceptron


 # Lecture des donnees
train = pd.read_csv ('mitbih_train.csv')
test = pd.read_csv ('mitbih_test.csv')
# Creation des ensembles train / test
X_train = train.iloc[:,0:-1]
X_test =  test.iloc[:,0:-1]
y_train = train.iloc[:,-1]
y_test = test.iloc[:,-1]

def stratified(_train_size):
    _X_train = pd.DataFrame()
    if _train_size >= 1:
        sss = StratifiedShuffleSplit(n_splits=1, train_size = 1)
    else:
        sss = StratifiedShuffleSplit(n_splits=1, train_size = _train_size)
    train_index, test_index = list(sss.split(X_train, y_train))[0]
    _X_train, _y_train = X_train.iloc[train_index, :], y_train[train_index]
    return _X_train, _y_train

def random(_value):
    # Il faut recupérer le nombre de data du fichier --> 87550 = exemple mitbih
    if _value > 87550:
        return X_train, y_train
    rows = np.random.choice(X_train.index.values, _value)
    _X_train = X_train.iloc[rows]
    _y_train = y_train.iloc[rows]
    return _X_train, _y_train

def split(_stratified, _random):
    if _stratified > 0:
        _X_train, _y_train = stratified(_stratified)
    elif _random > 0:
        _X_train, _y_train = random(_random)
    else:
        _X_train, _y_train = X_train, y_train
    return _X_train, _y_train
    
def printEvaluation(predictions):
    # Evaluation du classifieur
    print (classification_report(predictions,y_test))
    
def kNeighbors(k, _stratified, _random):
   # Echantillonnage
    _X_train, _y_train = split(_stratified, _random)
    # Creation du classifieur
    knn = KNeighborsClassifier ( n_neighbors =k)
    knn.fit(_X_train,_y_train )
    # Predictions
    predictions=knn.predict(X_test)
     # Evaluation du classifieur
    printEvaluation(predictions)

def perceptron(_tol, _n_iter_no_change, _stratified, _random):
    # Echantillonnage
    _X_train, _y_train = split(_stratified, _random)
    #Creation du classifieur
    clf = Perceptron(tol=_tol, n_iter_no_change=_n_iter_no_change)
    clf.fit(X_train, y_train)
    # Prediction
    predictions = clf . predict ( X_test )
    # Evaluation du classifieur
    printEvaluation(predictions)